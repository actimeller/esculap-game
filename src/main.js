import Vue from 'vue'
import App from './App.vue'


Vue.config.productionTip = false;

// Global components register.
import ButtonDiv from './components/ButtonDiv.vue'
import ButtonA from './components/ButtonA.vue'
import NickDialog from './components/NickDialog.vue'
import GameScores from './components/GameScores.vue'
import ButtonBack from './components/ButtonBack.vue'
import NextRule from './components/NextRule.vue'
import Rating from './components/Rating.vue'
import Game from './components/Game.vue'
import GameResult from './components/GameResult.vue'
import Slide from './components/Slide.vue'
import PreviousRule from './components/PreviousRule'



Vue.component('button-div', ButtonDiv);
Vue.component('nick-dialog', NickDialog);
Vue.component('button-a', ButtonA);
Vue.component('game-scores', GameScores);
Vue.component('button-back', ButtonBack);
Vue.component('previous-rule', PreviousRule);
Vue.component('next-rule', NextRule);
Vue.component('rating', Rating);
Vue.component('game', Game);
Vue.component('game-result', GameResult);
Vue.component('slide', Slide);

// Filters
Vue.filter('striphtml', function (value) {
    var div = document.createElement("div");
    div.innerHTML = value;
    var text = div.textContent || div.innerText || "";
    return text;
});

Vue.prototype.$axios = window.axios = require('axios');

// Specific headers for Laravel api;
if (typeof window.Laravel !== 'undefined') {
    Vue.prototype.$axios.defaults.headers.common['X-CSRF-TOKEN'] = window.Laravel.csrfToken;
    Vue.prototype.$axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
}

// EventBus for event handling across components and other vue app instances (if needed).
window.Event = new Vue();

// TODO remove usage of jquery and jquery-ui, make it more vanilla for vue.
window.$ = require('jquery');
require('jquery-ui-dist/jquery-ui.js');

const moment = require('moment');
require('moment/locale/ru');

Vue.use(require('vue-moment'), {
    moment
});

Vue.prototype.app = new Vue({
  render: h => h(App),
}).$mount('#app');


